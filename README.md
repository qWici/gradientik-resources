## About
Collection of gradients [Gradientik](https://gradientik.com)


## Contributing
Adding a gradient to the library is super simple. All the gradients are loaded and rendered from a single `gradients.json` file in the root.

To add your gradient, fork this repository, add your gradient colors in the HEX format along with a name to the end of the json file and submit a pull request. Don't forget the commas!

```
{
    "name": "My awesome gradient name",
    "colors": ["#cb202d", "#dc1e28", "#3366cc"]
}
```

*NOTE* - Please keep gradient submissions and bug fixes in separate PRs.

&nbsp;